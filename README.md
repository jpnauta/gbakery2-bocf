# gbakery2-bocf

Simple script to scrape the BOCF website. This code is hardcoded into the main Bakery site.

## Installation

```
yarn
```

## Build

```
yarn run build
```
