const Path = require('path');
const webpack = require('webpack');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = (options) => {
  const dest = Path.join(__dirname, '../dist');

  let webpackConfig = {
    mode: options.mode,
    devtool: options.devtool,
    entry: [
      './src/index'
    ],
    output: {
      path: dest,
      filename: 'bocf-script.js'
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify(options.isProduction ? 'production' : 'development')
        }
      }),
      new CleanWebpackPlugin({
        path: dest
      }),
      new webpack.ProvidePlugin({
        $: 'jqlite'
      })
    ],
    module: {
      rules: [{
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }]
    }
  };

  if (options.isProduction) {
    webpackConfig.entry = ['./src/index'];

    webpackConfig.plugins.push(
      new UglifyJSPlugin({
        sourceMap: true,
      }),
    );

  } else {
    webpackConfig.plugins.push(
      new webpack.HotModuleReplacementPlugin()
    );

    webpackConfig.devServer = {
      contentBase: dest,
      hot: true,
      port: options.port,
      inline: true
    };
  }

  return webpackConfig;

};
