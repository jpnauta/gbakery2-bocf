import _ from "lodash";
import {withIndex} from "./ext/lists";

const select = (element, query) => {
  // Sadly we can't use jqlite because we are interacting with a different `window`
  return element.querySelectorAll(query);
};

const text = (elements, separator = "") => {
  return _.map(elements, (e) => e.textContent).join(separator)
};

export const scrapePageCount = () => {
  const pageButtons = select(document, ".pagination li");
  if (pageButtons.length) {
    return Number(pageButtons[pageButtons.length - 2].textContent);
  }
  return 1;  // Only need to scrape the current page
};

export const scrapeOrders = (window) => {
  const tableRows = select(window.document, ".dokan-table tr");

  const relevantTableRows = _(tableRows)
    .map((row) => {
      return {
        isSummaryRow: !!select(row, ".dokan-order-id").length,
        isDetailRow: !!select(row, "table .item").length,
        row
      };
    })
    .filter(({isSummaryRow, isDetailRow}) => {
      // Remove irrelevant rows
      return isSummaryRow || isDetailRow;
    })
    .value();

  if (relevantTableRows.length % 2 !== 0) {
    throw new Error(`Expected even number of relevant table rows, got ${relevantTableRows.length}`);
  }

  if (!_(relevantTableRows)
    .filter((_, idx) => idx % 2 == 0)  // NOTE: idx is zero-indexed
    .every(['isSummaryRow', true])) {
    throw new Error("Expected all odd rows to be summary rows");
  }

  if (!_(relevantTableRows)
    .filter((_, idx) => idx % 2 == 1)  // NOTE: idx is zero-indexed
    .every(['isDetailRow', true])) {
    throw new Error("Expected all even rows to be detail rows");
  }

  const orders = _(relevantTableRows)
    .map("row")
    // Group into pairs
    .groupBy(withIndex((element, index) => {
      return Math.floor(index / 2);
    }))
    .map(([summaryRow, detailRow]) => {
      const deliveryDayDivs = select(summaryRow, ".dokan-order-select div");
      const deliveryDayText = text(deliveryDayDivs, " ");

      return {
        orderNo: Number(_.trim(text(select(summaryRow, ".dokan-order-id"))).split(" ")[1]),
        name: _.trim(text(select(summaryRow, '.dokan-order-customer'))),
        deliveryDay: _.trim(deliveryDayText),
        orderProducts: _.map(select(detailRow, "table tr.item"), (itemRow) => {
          const quantityText = select(itemRow, "td")[0].textContent;
          return {
            quantity: Number(_.trim(quantityText).split(" ")[0]),
            name: _.trim(text(select(itemRow, "td.name"))),
          };
        })

      };
    })
    .value();

  if (!_(orders)
    .map("orderNo")
    .every(_.isNumber)) {
    throw new Error("Expected all orderNos to be numbers");
  }

  if (!_(orders)
    .map("deliveryDay")
    .every(_.isString)) {
    throw new Error("Expected all deliveryDays to be strings");
  }

  if (!_(orders)
    .map("name")
    .every(_.isString)) {
    throw new Error("Expected all orderNos to be strings");
  }

  if (!_(orders)
    .map("orderProducts")
    .flatten()
    .map("quantity")
    .every(_.isNumber)) {
    throw new Error("Expected all order product quantities to be numbers");
  }

  if (!_(orders)
    .map("orderProducts")
    .flatten()
    .map("name")
    .every((op) => _.isString(op) && !_.isEmpty(op))) {
    throw new Error("Expected all order product names to be strings");
  }

  return orders;
};
