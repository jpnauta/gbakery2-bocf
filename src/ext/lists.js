export const withIndex = (fn) => {
  let index = 0
  return (thing) => fn(thing, index++)
}
