export const openWindowAsync = async (url) => {
  return new Promise((fulfill, reject) => {
    const newWindow = window.open(url);
    let opened = false;

    if (!newWindow) {
      throw new Error("New window was blocked! You likely need to enable popups\nhttps://support.google.com/chrome/answer/95472?co=GENIE.Platform%3DDesktop&hl=en-GB");
    }

    newWindow.onload = () => {
      fulfill(newWindow);
      opened = true;
    };
    newWindow.onbeforeunload = () => {
      if (!opened) {
        reject("New window was unexpectedly closed!");
      }
    };
  });
};
