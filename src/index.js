import {scrapeOrders, scrapePageCount} from "./scraping";
import {openWindowAsync} from "./ext/dom";

export const openScraperWindow = async (pagenum) => {
  const params = new URLSearchParams(window.location.search);
  params.set("pagenum", pagenum);
  const url = `${window.location.origin}${window.location.pathname}?${params.toString()}`;

  console.info(`Opening page ${url}`);
  return await openWindowAsync(url);
};

(async () => {
  let scraperWindow;
  try {
    console.clear();

    let pageNum = 0;
    let allOrders = [];

    const pageCount = scrapePageCount();
    console.info(`Found ${pageCount} pages to process`);

    while (pageNum < pageCount) {
      // Close existing page
      if (scraperWindow) {
        scraperWindow.close();
        scraperWindow = null;
      }

      // Open next page
      pageNum += 1;
      scraperWindow = await openScraperWindow(pageNum);
      scraperWindow.blur();
      window.focus();

      // Parse contents of page
      const orders = scrapeOrders(scraperWindow);
      allOrders = allOrders.concat(orders);
    }

    console.clear();
    console.log(`Successfully found ${allOrders.length} orders! Please copy-paste the text below back to the Bakery website.`);
    console.log("↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ COPY THIS ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓");
    console.log(JSON.stringify(allOrders));
    console.log("↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ COPY THIS ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑");

  } catch (e) {
    console.error(e);
    console.error("Failed to get order information - please read the error above. If the message doesn't make sense, take a screenshot and report it as an issue.");
  } finally {
    if (scraperWindow) {
      scraperWindow.close();
    }
  }
})();
